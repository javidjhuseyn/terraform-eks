data "aws_eks_cluster" "demo-eks-cluster" {
    name = module.demo-eks-cluster.cluster_id
}

data "aws_eks_cluster_auth" "demo-eks-cluster-auth" {
    name = module.demo-eks-cluster.cluster_id
} 

# Configure the AWS Provider
provider "kubernetes" {
  load_config_file = "false"
  host = data.aws_eks_cluster.demo-eks-cluster.endpoint
  token = data.aws_eks_cluster_auth.demo-eks-cluster-auth.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.demo-eks-cluster.certificate_authority.0.data)
}

module "demo-eks-cluster" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.20.0"
  
  cluster_name = "demo-eks-cluster"
  cluster_version = "~> 1.15"

  # list of subnets where worker nodes will start
  subnets = module.demo-eks-vpc.private_subnets

  tags = {
      environment = "demo"
  }

  vpc_id = module.demo-eks-vpc.vpc_id

  # configure worker nodes
  worker_groups = [
      {
          instance_type = "t2.micro"
          name = "worker-1"
          # number of this type
          asg_desired_capacity = 3
      }
  ]
}