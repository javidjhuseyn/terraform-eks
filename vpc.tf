variable vpc_cidr_block {}
variable private_subnet_cidr_blocks {}
variable public_subnet_cidr_blocks {}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "eu-west-2"
}

data "aws_availability_zones" "azs" {}

module "demo-eks-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.7.0"

  name = "demo-eks-vpc"
  cidr = var.vpc_cidr_block
  # best practice is to have one public and one private subnet in each az
  private_subnets = var.private_subnet_cidr_blocks
  public_subnets = var.public_subnet_cidr_blocks
  # dynamically set azs by querying data from aws
  azs = data.aws_availability_zones.azs.names
  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true

  tags = {
      "kubernetes.io/cluster/demo-eks-cluster" = "shared"
  }

  public_subnet_tags = {
      "kubernetes.io/cluster/demo-eks-cluster" = "shared"
      "kubernetes.io/role/elb" = 1
  }

  private_subnet_tags = {
      "kubernetes.io/cluster/demo-eks-cluster" = "shared"
      "kubernetes.io/role/internal-elb" = 1
  }

}